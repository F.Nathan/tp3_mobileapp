package com.example.uapv1700716.tp3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.uapv1700716.tp3.City;
import com.example.uapv1700716.tp3.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    private CityActivity cityActivity = this;
    private WeatherDbHelper weatherDbHelper = new WeatherDbHelper(getBaseContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        city = (City) getIntent().getParcelableExtra(City.TAG);

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AsyncTaskExample asyncTaskExample;
                asyncTaskExample = new AsyncTaskExample(cityActivity);
                asyncTaskExample.execute();

            }
        });

    }

    @Override
    public void onBackPressed() {
        final WeatherDbHelper dbHelper = new WeatherDbHelper(CityActivity.this);
        dbHelper.updateCity(city);
        super.onBackPressed();
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }


    public class AsyncTaskExample extends AsyncTask<Void, Void, Boolean> {

        // On a besoin du contexte pour replacer l'AsyncTask
        private Context context;
        // On récupère l'activité d'appel, au cas où besoin dans le traitement
        private Activity activity;

        /**
         * Constructeur de l'asyncTask.
         * @param
         */
        public AsyncTaskExample(Activity activity) {
            this.context = activity.getApplicationContext();
            this.activity = activity;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*
             * Cette fonction contiendra le code exécuté au préalable, par exemple:
             *  -Affichage d'une ProgressBar
             *      =rond qui tourne pour indiquer une attente
             *      =Barre de progression
             *  -...
             */
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            URL url = null;
            try {
                Log.d(TAG,"RGEGQBSGDBHSDGHBSDGIBSDFBISDFBNSDIBNSGDIBNSGDB");
                url = WebServiceUrl.build(city.getName(),city.getCountry());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                connection = (HttpURLConnection) url.openConnection();
                Log.d(TAG, "Connection : " + connection);
                connection.connect();
                InputStream in = new BufferedInputStream(connection.getInputStream());
                JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                jsonResponseHandler.readJsonStream(connection.getInputStream());
                return true;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            textCityName.setText(city.getName());
            textCountry.setText(city.getCountry());
            textTemperature.setText(city.getTemperature()+" °C");
            textHumdity.setText(city.getHumidity()+" %");
            textWind.setText(city.getFullWind());
            textCloudiness.setText(city.getHumidity()+" %");
            textLastUpdate.setText(city.getLastUpdate());

            if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
                Log.d(TAG,"icon="+"icon_" + city.getIcon());
                imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                        .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
                imageWeatherCondition.setContentDescription(city.getDescription());
            }


            return;
        }

        @Override
        protected void onCancelled() {
            return;
        }

    }




}
