package com.example.uapv1700716.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    WeatherDbHelper dbHelper = new WeatherDbHelper(this);
    City city = null;
    ListView listView = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (dbHelper.fetchAllCities().getCount() < 1) {
            dbHelper.populate();
        }
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY },
                new int[] { android.R.id.text1, android.R.id.text2});


        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                final Cursor selectedCity = (Cursor)adapter.getItemAtPosition(position);

                city = dbHelper.cursorToCity(selectedCity);
                Log.d("MAINACTIVITY", "city : " + city.toString());
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG, city);
                startActivity(intent);

            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                city = null;
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                intent.putExtra(City.TAG, city);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onResume(){
        super.onResume();
        final WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY },
                new int[] { android.R.id.text1, android.R.id.text2});
        final ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(cursorAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
